import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:math_game/menuGame.dart';
import 'package:provider/provider.dart';

import 'models/process_numbers.dart';

class SubPage extends StatefulWidget {
  @override
  _SubPageState createState() => _SubPageState();
}

class _SubPageState extends State<SubPage> {
  void chackAns(int index) {
    final counter = Provider.of<ProcessNumber>(context, listen: false);
    if (counter.num1 - counter.num2 == counter.arr[index]) {
      setState(() {
        counter.score++;
        counter.num1 = Random().nextInt(10);
        counter.num2 = Random().nextInt(10);
        counter.randomAns('-');
        counter.colors =
            List.generate(counter.arr.length, (_) => counter.getRandomColor());
      });
    } else {
      setState(() {
        counter.Hp--;
        counter.arr.removeAt(index);
        counter.colors.removeAt(index);
        if (counter.Hp == 0) {
          massDialog('หัวใจคุณหมดแล้ว :(');
        }
      });
    }
  }

  massDialog(String text) {
    final counter = Provider.of<ProcessNumber>(context, listen: false);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(text),
          content: Text('คะแนนของคุณคือ ${counter.score}'),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => menuGame()),
                );
              },
              child: Text(
                "กลับไปหน้าเมนู",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.red,
                ),
              ),
            ),
            //
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  counter.num1 = Random().nextInt(10);
                  counter.num2 = Random().nextInt(10);
                  counter.Hp = 3;
                  counter.score = 0;
                  counter.start = 30;
                  counter.randomAns('-');
                  counter.colors = List.generate(
                      counter.arr.length, (_) => counter.getRandomColor());
                  _startCountDown();
                });
              },
              child: Text(
                'เริ่มใหม่',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void _startCountDown() {
    final counter = Provider.of<ProcessNumber>(context, listen: false);
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (counter.start > 0) {
        setState(() {
          counter.start--;
        });
      } else {
        timer.cancel();
        if (counter.Hp != 0) {
          massDialog('Time Out!!');
        }
      }
      if (counter.Hp == 0) {
        timer.cancel();
      }
    });
  }

  @override
  void initState() {
    final counter = Provider.of<ProcessNumber>(context, listen: false);
    super.initState();
    counter.num1 = Random().nextInt(10);
    counter.num2 = Random().nextInt(10);
    counter.Hp = 3;
    counter.score = 0;
    counter.start = 30;
    counter.randomAns('-');
    counter.colors =
        List.generate(counter.arr.length, (_) => counter.getRandomColor());
    _startCountDown();
  }

  @override
  Widget build(BuildContext context) {
    final ProcessNumber counter = Provider.of<ProcessNumber>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('SUBTRACT'),
        backgroundColor: Colors.amber,
      ),
      body: Column(
        children: [
          //แถบคะแนน นับเวลาถอยหลัง พลังชีวิต :)
          Container(
            child: Row(
              children: [
                Expanded(
                  child: SizedBox(),
                ),
                Icon(Icons.favorite, color: Colors.red),
                SizedBox(width: 5),
                Text(
                  ': ${counter.Hp}',
                  style: TextStyle(fontSize: 18),
                ),
                Expanded(
                  child: SizedBox(),
                ),
                Text(
                  'Score: ${counter.score}',
                  style: TextStyle(fontSize: 18),
                ),
                Expanded(
                  child: SizedBox(),
                ),
                Icon(Icons.timer),
                SizedBox(width: 5),
                Text(
                  ': ${counter.start}',
                  style: TextStyle(fontSize: 18),
                ),
                Expanded(
                  child: SizedBox(),
                ),
              ],
            ),
            decoration: BoxDecoration(
              // color: Colors.deepPurple,
              borderRadius: BorderRadius.circular(6),
            ),
            height: 50,
            width: double.infinity,
          ),
          //คำถาม :)
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  counter.num1.toString() + ' - ' + counter.num2.toString(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 60,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          //ปุ่มกดคำตอบ :)
          Expanded(
              flex: 2,
              child: GridView.builder(
                  itemCount: counter.arr.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: MediaQuery.of(context).size.width /
                        (MediaQuery.of(context).size.height / 2.3),
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                        onTap: () {
                          chackAns(index);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            alignment: Alignment.center,
                            child: Text('${counter.arr[index]}',
                                style: TextStyle(
                                    fontSize: 40, color: Colors.white)),
                            decoration: BoxDecoration(
                              color: counter.colors[index],
                              borderRadius: BorderRadius.circular(6),
                            ),
                          ),
                        ));
                  })),
        ],
      ),
    );
  }
}
