import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../menuGame.dart';

class ProcessNumber with ChangeNotifier {
  late List<int?> arr = [
    null,
    null,
    null,
    null,
  ];
  late List<double?> arr2 = [
    null,
    null,
    null,
    null,
  ];
  late List<Color> colors = List.generate(arr.length, (_) => getRandomColor());

  late BuildContext context;
  ProcessNumber(this.context);
  Color getRandomColor() {
    return Color((Random().nextDouble() * 0xFFFFFF).toInt() << 0)
        .withOpacity(1.0);
  }

  int num1 = Random().nextInt(10);
  int num2 = Random().nextInt(10);
  int Hp = 3;
  int score = 0;
  int start = 30;

  void randomAns(String text) {
    arr = [
      null,
      null,
      null,
      null,
    ];
    arr2 = [
      null,
      null,
      null,
      null,
    ];
    if (text == '-') {
      arr[Random().nextInt(4)] = num1 - num2;
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] == null) {
          int newNum;
          bool isUnique = false;
          while (!isUnique) {
            if (Random().nextInt(2) == 1) {
              newNum = num1 - num2 + Random().nextInt(10);
            } else {
              newNum = num1 - num2 - Random().nextInt(10);
            }
            if (!arr.contains(newNum)) {
              arr[i] = newNum;
              isUnique = true;
            }
          }
        }
      }
    } else if (text == '/') {
      arr2[Random().nextInt(4)] = num1 / num2;
      for (var i = 0; i < arr2.length; i++) {
        if (arr2[i] == null) {
          double newNum;
          bool isUnique = false;
          while (!isUnique) {
            if (Random().nextInt(2) == 1) {
              newNum = (num1 / num2) + Random().nextInt(8) + 1;
            } else {
              newNum = (num1 / num2) - Random().nextInt(8) + 1;
            }
            if (!arr2.contains(newNum)) {
              arr2[i] = newNum;
              isUnique = true;
            }
          }
        }
      }
    } else if (text == '+') {
      arr[Random().nextInt(4)] = num1 + num2;
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] == null) {
          int newNum;
          bool isUnique = false;
          while (!isUnique) {
            if (Random().nextInt(2) == 1) {
              newNum = num1 + num2 + Random().nextInt(10);
            } else {
              newNum = num1 + num2 - Random().nextInt(10);
            }
            if (!arr.contains(newNum)) {
              arr[i] = newNum;
              isUnique = true;
            }
          }
        }
      }
    } else if (text == '*') {
      arr[Random().nextInt(4)] = num1 * num2;
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] == null) {
          int newNum;
          bool isUnique = false;
          while (!isUnique) {
            if (Random().nextInt(2) == 1) {
              newNum = num1 * num2 + Random().nextInt(10);
            } else {
              newNum = num1 * num2 - Random().nextInt(10);
            }
            if (!arr.contains(newNum)) {
              arr[i] = newNum;
              isUnique = true;
            }
          }
        }
      }
    }
    notifyListeners(); // เพื่อให้ Consumer ที่ใช้งานอัพเดตข้อมูล
  }
}
