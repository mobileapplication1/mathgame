import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:math_game/menuGame.dart';
import 'Plus.dart';
import 'package:provider/provider.dart';
import 'models/process_numbers.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => ProcessNumber(context), 
      child: DevicePreview(
        tools: const [
          DeviceSection(),
        ],
        builder: (context) => const ExampleApp(),
      ),
    ),
  );
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      useInheritedMediaQuery: true,
      builder: DevicePreview.appBuilder,
      locale: DevicePreview.locale(context),
      title: 'Math Flutter Game',
      home: menuGame(),
    );
  }
}
