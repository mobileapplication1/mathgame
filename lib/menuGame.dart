import 'package:flutter/material.dart';
import 'package:math_game/Plus.dart';

import 'Divide.dart';
import 'Sub.dart';
import 'multi.dart';

class menuGame extends StatelessWidget {
  const menuGame({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.pink[300],
        automaticallyImplyLeading: false,
        title: Text(
          "Math Land",
          style: TextStyle(color: Colors.white, fontSize: 24),
        ),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 300.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PlusPage()));
                },
                child: const Text('Plus'),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  minimumSize: Size(150, 50),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.red,
                ),
              ),
              SizedBox(height: 15),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SubPage()));
                },
                child: const Text('Subtract'),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  minimumSize: Size(150, 50),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.amber,
                ),
              ),
              SizedBox(height: 15),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MultiPage()));
                },
                child: const Text('Multiply'),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  minimumSize: Size(150, 50),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.orange,
                ),
              ),
              SizedBox(height: 15),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DividePage()));
                },
                child: const Text('Divide'),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  minimumSize: Size(150, 50),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.pink,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
